#include <iostream>
#include <vector>
#include <sstream>
#include <algorithm>
#include <fstream>
#include <windows.h>

using namespace std;

struct User
{
    int id;
    string loginName, password;
};

enum SearchType
{
    FIRST_NAME, LAST_NAME
};

struct Person
{
    int id;
    int userID;
    string name;
    string surname;
    string email;
    string phoneNumber;
    string address;
};

string readLine()
{
    string input;
    cin.sync();
    getline(cin, input);
    return input;
}

char readSign()
{
    char sign;
    string input;

    while(true)
    {
        input = readLine();

        if (input.length() ==1)
        {
            sign = input[0];
            break;
        }
        cout << "This is not a single sign. Try again. " << endl;
    }
    return sign;
}

int readInteger()
{
    string input;
    int number;

    while(true)
    {
        input = readLine();
        stringstream myStream(input);

        if (myStream >> number)
        {
            break;
        }
        cout << "This is not a number. Try again: \n";
    }
    return number;
}

string getString(char ch)
{
    string letter(1, ch);
    return letter;
}

string extractFromLine(string longText, int previousWordLastLetterPlusOne)
{
    string text = "";
    char letter;
    int i = previousWordLastLetterPlusOne;

    while (letter != '|')
    {
        text = text + getString(longText[i]);
        i++;
        letter = longText[i];
    }
    return text;
}

int saveContactsAfterDelete(int idOfDeletedContact)
{
    int lastID = 0;
    string lineFromFile, field;
    string oldFileName = "AllContacts.txt";
    string tempFileName = "TemporaryContacts.txt";
    ifstream file(oldFileName);
    ofstream tempfile(tempFileName);

    while (getline(file, lineFromFile))
    {
        istringstream iss(lineFromFile);

        getline(iss, field, '|');

        if (stoi(field) != idOfDeletedContact)
        {
            tempfile << lineFromFile << endl;
            lastID = stoi(field);
        }
    }

    file.close();
    tempfile.close();
    remove("AllContacts.txt");
    rename("TemporaryContacts.txt", "AllContacts.txt");

    cout << "Contact deleted" << endl;
    system("pause");
    return lastID;
}

void saveContactsAfterChange(const Person &oneEntry, int lastID)
{
    fstream addressbook;
    addressbook.open("AllContacts.txt", ios::in);

    fstream temporarybook;
    FILE *temporaryFile = fopen("TemporaryContacts.txt", "w");
    fclose(temporaryFile);
    temporarybook.open("TemporaryContacts.txt", ios::in | ios::app);

    string text, lineFromAll;
    int contactIDFromAll = 0;
    int contactIDChanged = oneEntry.id;

    while (contactIDFromAll < lastID)
    {
        getline(addressbook, lineFromAll);
        text = extractFromLine(lineFromAll, 0);
        contactIDFromAll = stoi(text);

        if (contactIDFromAll != contactIDChanged)
        {
            temporarybook << lineFromAll << endl;
        }
        else
        {
            temporarybook << oneEntry.id << "|" << oneEntry.userID  << "|" << oneEntry.name << "|" << oneEntry.surname << "|" << oneEntry.email << "|" << oneEntry.phoneNumber << "|" <<oneEntry.address << "|" << endl;
        }
    }

    addressbook.close();
    temporarybook.close();
    remove("AllContacts.txt");
    rename("TemporaryContacts.txt", "AllContacts.txt");
}

void addNewContactToFile(const Person &oneEntry)
{
    ofstream fileContacts("AllContacts.txt", ios::app);

    if (!fileContacts.is_open())
    {
        cout << "Error: Failed to open file" << endl;
        return;
    }

    fileContacts << oneEntry.id << "|" << oneEntry.userID  << "|" << oneEntry.name << "|" << oneEntry.surname << "|" << oneEntry.email << "|" << oneEntry.phoneNumber << "|" <<oneEntry.address << "|" << endl;

    fileContacts.close();
}

int getContactsFromFile(vector <Person> &contacts, const int loggedUserID)
{
    fstream addressbook;
    int lastID = 0;
    addressbook.open("AllContacts.txt", ios::in | ios::app);

    contacts.clear();

    if (!addressbook.is_open())
    {
        cout << "Failed to open file" << endl;
        return lastID;
    }

    string lineFromFile;
    string text;
    Person individualPerson;

    while (getline(addressbook, lineFromFile))
    {
        istringstream iss(lineFromFile);

        getline(iss, text, '|');
        individualPerson.id = stoi(text);
        lastID = stoi(text);

        getline(iss, text, '|');
        individualPerson.userID = stoi(text);

        if (individualPerson.userID != loggedUserID)
        {
            continue;
        }

        getline(iss, individualPerson.name, '|');
        getline(iss, individualPerson.surname, '|');
        getline(iss, individualPerson.email, '|');
        getline(iss, individualPerson.phoneNumber, '|');
        getline(iss, individualPerson.address, '|');

        contacts.push_back(individualPerson);
    }

    addressbook.close();
    return lastID;
}

void displayFirstMenu()
{
    system("cls");
    cout << "1. Registration \n";
    cout << "2. Logging in \n";
    cout << "9. End \n";
}

void displayMenu()
{
    system("cls");
    cout << ">>> CONTACT BOOK <<<" << endl << endl;
    cout << "1. Add contact \n";
    cout << "2. Search by name \n";
    cout << "3. Search by surname \n";
    cout << "4. Show all contacts \n";
    cout << "5. Delete contact \n";
    cout << "6. Edit contact \n";
    cout << "7. Change your password \n";
    cout << "8. Log out \n";
}

void displayOnePersonData(Person oneEntry)
{
    cout << " ID: " << oneEntry.id << endl;
    cout << " Name: " << oneEntry.name <<endl;
    cout << " Surname: " << oneEntry.surname << endl;
    cout << " email: " << oneEntry.email << endl;
    cout << " phone: " << oneEntry.phoneNumber << endl;
    cout << " address: " << oneEntry.address << endl;
    return;
}

int checkIfContactExists(vector <Person> &contacts, int ID)
{
    int contactNumber = -1;

    for (size_t i = 0; i < contacts.size(); i++)
    {
        if (contacts[i].id == ID)
        {
            contactNumber = i;
        }
    }

    return contactNumber;
}

void addNewEntry(vector <Person> &contacts, int userID, int lastID)
{
    Person individualPerson;

    cout << "Put name: ";
    individualPerson.name = readLine();
    cout << "Put surname: ";
    individualPerson.surname = readLine();
    cout << "Put email: ";
    individualPerson.email = readLine();
    cout << "Put phone: ";
    individualPerson.phoneNumber = readLine();
    cout << "Put address: ";
    individualPerson.address = readLine();
    individualPerson.id = lastID + 1;
    individualPerson.userID = userID;

    contacts.push_back(individualPerson);
    addNewContactToFile(individualPerson);
}

int deleteEntry(vector <Person> &contacts, int userID, int lastID)
{
    int idToBeDeleted, contactNumber;
    string confirmation;

    cout << "Put ID of the contact to delete: \n";
    idToBeDeleted = readInteger();
    contactNumber = checkIfContactExists(contacts, idToBeDeleted);

    if (contactNumber >= 0)
    {
        displayOnePersonData(contacts[contactNumber]);
        cout << "Confirm if you want to delete the contact - put t: ";
        confirmation = readLine();

        if (confirmation == "t")
        {
            contacts.erase(remove_if(contacts.begin(), contacts.end(), [&](const Person &contact)
            {
                return contact.id == idToBeDeleted;
            }),
            contacts.end());
            lastID = saveContactsAfterDelete(idToBeDeleted);
            return lastID;
        }
    }
    else
    {
        cout << "There is no such contact.\n";
        system("pause");
        return lastID;
    }
    return lastID;
}

void changeEntry(vector <Person> &contacts, int idToChange, int choice)
{
    int lastID = contacts.back().id;
    int indexOfContactToChange;

    for (int i = 0; i < lastID; i++)
    {
        if (contacts[i].id == idToChange)
        {
            indexOfContactToChange = i;
        }
    }

    switch(choice)
    {
    case 1:
        cout << "Put name: ";
        contacts[indexOfContactToChange].name = readLine();
        break;
    case 2:
        cout << "Put surname: ";
        contacts[indexOfContactToChange].surname = readLine();
        break;
    case 3:
        cout << "Put phone number: ";
        contacts[indexOfContactToChange].phoneNumber = readLine();
        break;
    case 4:
        cout << "Put email: ";
        contacts[indexOfContactToChange].email = readLine();
        break;
    case 5:
        cout << "Put address: ";
        contacts[indexOfContactToChange].address = readLine();
        break;
    }

    system("cls");
    cout << "The entry is changed. \n";
    displayOnePersonData(contacts[indexOfContactToChange]);
    saveContactsAfterChange(contacts[indexOfContactToChange], lastID);
}

void chooseChangeOfEntry(vector <Person> &contacts, int userID, int lastID)
{
    int ID, choice, contactNumber;

    cout << " Put ID of a contact to be changed: ";
    ID = readInteger();

    contactNumber = checkIfContactExists(contacts, ID);

    if (contactNumber == -1)
    {
        cout << "There is no such id.\n";
    }
    else
    {
        Person oneEntry = contacts[contactNumber];

        displayOnePersonData(oneEntry);

        while (true)
        {
            cout << endl << endl << " What to change? Choose a number:  \n";
            cout << "1. Name \n";
            cout << "2. Surname \n";
            cout << "3. Phone \n";
            cout << "4. Email \n";
            cout << "5. Address \n" <<endl;
            cout << "6. Back to the main menu \n";
            choice = readInteger();

            switch (choice)
            {
            case 6:
                return;
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
                changeEntry(contacts, ID, choice);
                Sleep(500);
                break;
            default:
                cout << "Try again with a number: ";
            }
        }
    }
    system("pause");
}

void lookForNameOrSurname(vector <Person> &contacts, SearchType choice)
{
    string nameToSearch;
    bool contactExists = false;

    cout << "Who do you want to search?: \n";
    nameToSearch = readLine();

    for (size_t i = 0; i < contacts.size(); i++)
    {
        if ((choice == FIRST_NAME && contacts[i].name == nameToSearch) || (choice != FIRST_NAME && contacts[i].surname == nameToSearch))
        {
            displayOnePersonData(contacts[i]);
            contactExists = true;
        }
    }

    if (contactExists == false)
    {
        cout << "There is no such a person.\n";
    }

    system("pause");
}

void printAllContacts(vector <Person> names)
{
    bool checkingIfContactsExist = false;

    for (Person oneEntry : names)
    {
        cout << endl;
        displayOnePersonData(oneEntry);
        checkingIfContactsExist = true;
    }

    if (checkingIfContactsExist == false)
    {
        cout << "There are no contacts here, so far.\n";
    }

    system("pause");
}

void getUsersFromFile(vector <User> &users)
{
    string lineFromFile, text;
    User individualUser;
    ifstream fileWithUsers("Users.txt", ios::in | ios::app);

    if (!fileWithUsers.is_open())
    {
        cout << "Error: Failed to open file" << endl;
        return;
    }

    while (getline(fileWithUsers, lineFromFile))
    {
        istringstream iss(lineFromFile);
        getline(iss, text, '|');
        individualUser.id = stoi(text);
        getline(iss, individualUser.loginName, '|');
        getline(iss, individualUser.password, '|');
        users.push_back(individualUser);
    }
    fileWithUsers.close();
}

void saveUsersToFile(vector <User> users)
{
    fstream usersOfAdrressBook;
    fclose(fopen("Users.txt", "w"));
    usersOfAdrressBook.open("Users.txt");
    for (User oneEntry : users)
    {
        usersOfAdrressBook << oneEntry.id << "|" << oneEntry.loginName << "|" << oneEntry.password << "|" << endl;
    }
}

void saveUserToFile(const User &oneEntry)
{
    ofstream fileUsers("Users.txt", ios::app);

    if (!fileUsers.is_open())
    {
        cout << "Failed to open file" << endl;
        return;
    }

    fileUsers << oneEntry.id << "|" << oneEntry.loginName << "|" << oneEntry.password << "|" << endl;

    fileUsers.close();
}

void registerUser (vector <User> &users)
{
    User user, lastUser;
    string login, password;
    int lastID = users.empty() ? 0 : users.back().id;

    cout << "Set up your new credentials\n login: ";
    login = readLine();

    int i = 0;

    while (i < lastID)
    {
        if (users[i].loginName == login)
        {
            cout << "Such a user exists. Try again: ";
            login = readLine();
            i = 0;
        }
        else
        {
            i++;
        }
    }

    cout << "Set up your new credentials\n password: ";
    password = readLine();

    user.loginName = login;
    user.password = password;
    user.id = lastID + 1;
    users.push_back(user);

    cout << "Account has been set up. \n";
    saveUserToFile(user);
    Sleep(1000);
}

int logUserInto(vector <User> &users)
{
    User user;
    string userName, password;
    int usersCount = users.size();
    cout << "Put your login in order to log in: \n";
    userName = readLine();

    int i = 0;

    while (i < usersCount)
    {
        if (users[i].loginName == userName)
        {
            for (int trials = 0; trials < 3; trials++)
            {
                cout << "Put password. You can try " << 3 - trials << " more.\n";
                password = readLine();
                if (users[i].password == password)
                {
                    cout << "You are logged in.\n";
                    Sleep(1000);
                    return users[i].id;
                }
            }
            cout << "You have already put a wrong password for 3 times. Wait a few seconds and try again. \n";
            Sleep(3000);
            return -1;
        }
        else
        {
            i++;
        }

    }
    cout << "There is no user with such a login. \n";
    Sleep(1500);
    return -1;
}

void changePassword(User &user)
{
    cout << "Put new password: ";
    user.password = readLine();
    cout << " Password has been successfully changed. \n";
    Sleep(1500);
}

int main()
{
    vector <Person> myContacts;
    vector <User> users;
    int idOfUser = 0;
    int lastID;
    char choice;

    getUsersFromFile(users);

    while(1)
    {
        while (idOfUser == 0)
        {
            displayFirstMenu();
            choice = readSign();

            if (choice == '1')
            {
                registerUser(users);
            }
            else if (choice == '2')
            {
                idOfUser = logUserInto(users);
                lastID = getContactsFromFile(myContacts, idOfUser);
            }
            else if (choice == '9')
            {
                exit(0);
            }
        }

        while (idOfUser != 0)
        {
            displayMenu();
            choice = readSign();

            switch(choice)
            {
            case '1':
                addNewEntry(myContacts, idOfUser, lastID);
                lastID++;
                break;
            case '2':
                lookForNameOrSurname(myContacts, FIRST_NAME);
                break;
            case '3':
                lookForNameOrSurname(myContacts, LAST_NAME);
                break;
            case '4':
                printAllContacts(myContacts);
                break;
            case '5':
                lastID = deleteEntry(myContacts, idOfUser, lastID);
                break;
            case '6':
                chooseChangeOfEntry(myContacts, idOfUser, lastID);
                break;
            case '7':
                changePassword(users[idOfUser-1]);
                saveUsersToFile(users);
                break;
            case '8':
                idOfUser = 0;
                break;
            }
        }
    }
    return 0;
}
